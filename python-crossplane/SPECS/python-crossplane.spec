# Created by pyp2rpm-3.3.7
%global pypi_name crossplane
%global pypi_version 0.5.7

Name:           python-%{pypi_name}
Version:        %{pypi_version}
Release:        1%{?dist}
Summary:        Reliable and fast NGINX configuration file parser

License:        Apache 2.0
URL:            https://github.com/nginxinc/crossplane
Source0:        %{pypi_source}
BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  python3dist(setuptools)

%description
![Crossplane Logo]( <h1 align"center">crossplane</h1> <h3
align"center">Reliable and fast NGINX configuration file parser and
builder</h3><p align"center"> <a href" src" <a href" src" <a href" src" <a
href" src"

%package -n     python3-%{pypi_name}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{pypi_name}}

Requires:       python3dist(setuptools)
%description -n python3-%{pypi_name}
![Crossplane Logo]( <h1 align"center">crossplane</h1> <h3
align"center">Reliable and fast NGINX configuration file parser and
builder</h3><p align"center"> <a href" src" <a href" src" <a href" src" <a
href" src"


%prep
%autosetup -n %{pypi_name}-%{pypi_version}
# Remove bundled egg-info
rm -rf %{pypi_name}.egg-info

%build
%py3_build

%install
%py3_install

%check
%{__python3} setup.py test

%files -n python3-%{pypi_name}
%license LICENSE
%doc README.md
%{_bindir}/crossplane
%{python3_sitelib}/%{pypi_name}
%{python3_sitelib}/%{pypi_name}-%{pypi_version}-py%{python3_version}.egg-info

%changelog
* Mon Dec 06 2021 mockbuilder - 0.5.7-1
- Initial package.