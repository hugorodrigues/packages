#!/bin/bash
# Builds SRPM package for Fedora COPR

set -e
set -x

OUTDIR="${1}"
SPEC="${2}"

TOPDIR="$(pwd)"

dnf install -y rpmdevtools

mkdir -p SOURCES SRPMS

# COPR pass SPEC as TOPDIR if unset
if [ -d "${SPEC}" ]; then
    SPEC="$(find SPECS -name "*.spec" -type f | head -n 1)"
fi
spectool --get-files --all --directory SOURCES "${SPEC}"
rpmbuild --define '_topdir .' -bs "${SPEC}"

find "SRPMS" -type f -exec mv {} "${OUTDIR}/" \;
